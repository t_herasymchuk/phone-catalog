package ua.test.phonecatalog.orderservice.exceptions;

public class PhonesNotFoundException extends RuntimeException {

	public PhonesNotFoundException(String msg) {
		super(msg);
	}
	
}
