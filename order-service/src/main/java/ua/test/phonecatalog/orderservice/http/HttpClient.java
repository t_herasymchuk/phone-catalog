package ua.test.phonecatalog.orderservice.http;

import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import ua.test.phonecatalog.orderservice.model.Phone;

@Component
public class HttpClient {

	@Value("${phone-catalog.service.url}")
	private String phoneCatalogServiceUrl;
	
	private final WebClient webClient;
	
	public HttpClient() {
		this.webClient = WebClient.create(this.phoneCatalogServiceUrl);
	}
	
	public Flux<Phone> retrievePhones(final List<Long> idSeq) {
		return this.webClient
				.get()
				.uri("/phones?idSeq={idSeq}", idSeq)
				.retrieve()
				.bodyToFlux(Phone.class);
	}
	
}
