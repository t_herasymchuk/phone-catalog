package ua.test.phonecatalog.catalogservice.service;

import java.util.Set;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ua.test.phonecatalog.catalogservice.model.Phone;
import ua.test.phonecatalog.catalogservice.repository.PhoneCatalogRepository;

@Service
public class PhoneCatalogService {

	private final PhoneCatalogRepository phoneCatalogRepository;

	public PhoneCatalogService(final PhoneCatalogRepository phoneCatalogRepository) {
		this.phoneCatalogRepository = phoneCatalogRepository;
	}

	public Flux<Phone> find() {
		return this.phoneCatalogRepository.findAll();
	}

	public Flux<Phone> find(final Set<Long> idSequence) {
		return this.phoneCatalogRepository.findAllById(idSequence);
	}

	public Mono<Phone> save(final Phone phone) {
		return this.phoneCatalogRepository.save(phone);
	}

}
