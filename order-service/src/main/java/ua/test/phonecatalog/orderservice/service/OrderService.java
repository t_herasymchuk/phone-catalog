package ua.test.phonecatalog.orderservice.service;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ua.test.phonecatalog.orderservice.exceptions.PhonesNotFoundException;
import ua.test.phonecatalog.orderservice.http.HttpClient;
import ua.test.phonecatalog.orderservice.model.Order;
import ua.test.phonecatalog.orderservice.model.Phone;
import ua.test.phonecatalog.orderservice.repository.OrderRepository;

@Service
public class OrderService {

	private final OrderRepository orderRepository;
	private final HttpClient httpClient;
	
	public OrderService(final OrderRepository orderRepository, final HttpClient httpClient) {
		this.orderRepository = orderRepository;
		this.httpClient = httpClient;
	}
	
	public Mono<Order> save(final Order order) {
		final List<Long> phoneIds = order.getPhoneIds();
		return this.httpClient.retrievePhones(phoneIds)
			.switchIfEmpty(Mono.error(new PhonesNotFoundException("Failed to retrieve the phones with given ids, idSeq=" + phoneIds)))
			.map(Phone::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add)
			.flatMap(sum -> {
				order.setTotalPrice(sum);
				return this.orderRepository.save(order);
			});
	}
	
	public Mono<Order> find(final Long id) {
		return this.orderRepository.findById(id);
	}
	
}
