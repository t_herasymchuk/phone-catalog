package ua.test.phonecatalog.orderservice.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "orders")
public class Order {

	@Id
	private Long id;

	@NotBlank
	private String customerName;

	@NotBlank
	private String customerSurname;

	@Email
	@NotBlank
	private String email;
	
	private BigDecimal totalPrice;

	@NotEmpty
	private List<Long> phoneIds;
	
	public Order() {
		
	}

	public Order(Long id, @NotBlank String customerName, @NotBlank String customerSurname,
			@Email @NotBlank String email, @NotEmpty List<Long> phoneIds, BigDecimal totalPrice) {
		super();
		this.id = id;
		this.customerName = customerName;
		this.customerSurname = customerSurname;
		this.email = email;
		this.phoneIds = phoneIds;
		this.totalPrice = totalPrice;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerSurname() {
		return customerSurname;
	}

	public void setCustomerSurname(String customerSurname) {
		this.customerSurname = customerSurname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Long> getPhoneIds() {
		return phoneIds;
	}

	public void setPhoneIds(List<Long> phoneIds) {
		this.phoneIds = phoneIds;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Order phone = (Order) o;
		return Objects.equals(id, phone.id) && Objects.equals(email, phone.email)
				&& Objects.equals(customerName, phone.customerName)
				&& Objects.equals(customerSurname, phone.customerSurname)
				&& Objects.equals(phoneIds, phone.phoneIds);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, email, customerName, customerSurname, phoneIds);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Order{");
		sb.append("id=").append(id);
		sb.append(", customerName=").append(customerName);
		sb.append(", customerSurname=").append(customerSurname);
		sb.append(", email=").append(email);
		sb.append(", phoneIds=").append(phoneIds).append("}");
		return sb.toString();
	}



}
